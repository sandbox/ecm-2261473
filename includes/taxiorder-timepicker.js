/**
 * @file
 * Some JavaScript to use Datepicker and Timepicker plugins.
 */

(function ($) {
  $(document).ready(function() {

    $('#edit-taxiorder-date').datepicker({
      numberOfMonths: 2,
      minDate: 0
    });

    $('#edit-taxiorder-date').datepicker('option', 'dateFormat', 'dd.mm.yy');
    $('#edit-taxiorder-date').datepicker('option', 'firstDay', 1);

    // See http://fgelinas.com/code/timepicker/
    $('#edit-taxiorder-time').timepicker({
      timeSeparator: ':',
      showLeadingZero: true,
      showMinutesLeadingZero: true,
      showPeriod: false,
      showPeriodLabels: false,
      defaultTime: '',
      showOn: 'focus',
      minutes: {
          starts: 0,
          ends: 55,
          interval: 5
      },
      rows: 4,
      showCloseButton: false,
      showNowButton: false,
      showDeselectButton: false
    });

  });
} (jQuery));
