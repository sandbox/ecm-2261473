/**
 * @file
 * Google Maps and Stored Routes functions.
 */

(function ($) {
  $(document).ready(function() {

    var settings = Drupal.settings['taxiorder'];

    var gmap_center = settings['gmap_center'];
    var gmap_swbound = settings['gmap_swbound'];
    var gmap_nebound = settings['gmap_nebound'];

    // Separate coordinates.
    arr_center = gmap_center.split(',');
    var center_lat = $.trim(arr_center[0]);
    var center_lng = $.trim(arr_center[1]);
    arr_swbound = gmap_swbound.split(',');
    var swbound_lat = $.trim(arr_swbound[0]);
    var swbound_lng = $.trim(arr_swbound[1]);
    arr_nebound = gmap_nebound.split(',');
    var nebound_lat = $.trim(arr_nebound[0]);
    var nebound_lng = $.trim(arr_nebound[1]);

    var map;
    var geocoder;
    var directionsService;
    var directionsDisplay;
    var moscow = new google.maps.LatLng(center_lat, center_lng);
    var swbound = new google.maps.LatLng(swbound_lat, swbound_lng);
    var nebound = new google.maps.LatLng(nebound_lat, nebound_lng);
    var markerPickup = new google.maps.Marker();

    initialize_map();
    drawRoute();

    $("#edit-taxiorder-pickup-street")
      .keydown(function(e) {
        // Handle Enter.
        if (e.which == 13) {
          codePickupAddress();
          return false;
        }
      })
      .blur(function() {
        codePickupAddress();
      });

    $("#edit-taxiorder-destination-street")
      .keypress(function(e) {
        if ($('input[name="marker_pickup"]').val()) {
          $("#edit-taxiorder-show-route").removeAttr("disabled");
          $("#edit-taxiorder-show-route").removeClass("form-button-disabled");
          $("#edit-taxiorder-show-route").css("display", "block");
        }
      })
      .keydown(function(e) {
        // Handle Enter.
        if (e.which == 13) {
          codeDestinationAddress();
          return false;
        }
      })
      .blur(function() {
        codeDestinationAddress();
      });

    $("#edit-taxiorder-show-route").click(function() {
      if ($('input[name="marker_pickup"]').val() && $("#edit-taxiorder-destination-street").val()) {
        codeDestinationAddress();
      }
      return false;
    });

    $("#edit-taxiorder-pickup-address-type").change(function() {
      $('select[id^="edit-taxiorder-pickup-poi-"]').val('');
      $("#edit-taxiorder-pickup-street").val('');
      show_moscow();
    });
    $("#edit-taxiorder-destination-address-type").change(function() {
      $('select[id^="edit-taxiorder-destination-poi-"]').val('');
      $("#edit-taxiorder-destination-street").val('');
      if ($(this).val() == 0) {
        $("#edit-taxiorder-show-route").css("display", "none");
      }
      if ($('input[name="marker_pickup"]').val()) {
        codePickupAddress();
      }
      else {
        show_moscow();
      }
    });

    // Stored Routes.
    $('#edit-stored-route').change(function() {
      var rid = $('#edit-stored-route').val();
      if (rid > 0) {
        var p_addr_type = settings['stored_routes'][rid]['pickup_address_type'];
        $('#edit-taxiorder-pickup-address-type').val(p_addr_type).change();

        if ($.isNumeric(p_addr_type) && (p_addr_type > 0)) {
          $('#edit-taxiorder-pickup-poi-' + p_addr_type).val(settings['stored_routes'][rid]['pickup_poi']);
        }
        else if (p_addr_type == 'not_a_poi') {
          $('#edit-taxiorder-pickup-street').val(settings['stored_routes'][rid]['pickup_street']);
        }

        $('input:[name="marker_pickup"]').val(settings['stored_routes'][rid]['marker_pickup']);
        $('#edit-taxiorder-pickup-info').val(settings['stored_routes'][rid]['pickup_info']);

        var d_addr_type = settings['stored_routes'][rid]['destination_address_type'];
        $('#edit-taxiorder-destination-address-type').val(d_addr_type).change();

        if ($.isNumeric(d_addr_type) && (d_addr_type > 0)) {
          $('#edit-taxiorder-destination-poi-' + d_addr_type).val(settings['stored_routes'][rid]['destination_poi']).change();
        }
        else if (d_addr_type == 'not_a_poi') {
          $('#edit-taxiorder-destination-street').val(settings['stored_routes'][rid]['destination_street']);
        }

        if ($('input[name="marker_pickup"]').val()) {
          drawRoute();
        }
        else {
          show_moscow();
        }
      }
    });

    // POIs.
    $('select[id^="edit-taxiorder-pickup-poi-"]').change(function() {
      var poi_id = $(this).val();

      if ($.isNumeric(poi_id)) {
        $.ajax({
          url: 'poi',
          type: 'POST',
          data: ({poi_id:poi_id}),
          dataType: 'json'
        })
        .done(function(data) {
            $('#edit-taxiorder-pickup-street').val(data['street']);
            $('input[name="marker_pickup"]').val(data['google_coordinates']);
            codePickupAddress();
        });
      }
    });
    $('select[id^="edit-taxiorder-destination-poi-"]').change(function() {
      var poi_id = $(this).val();

      if ($.isNumeric(poi_id)) {
        $.ajax({
          url: 'poi',
          type: 'POST',
          data: ({poi_id:poi_id}),
          dataType: 'json'
        })
        .done(function(data) {
            $('#edit-taxiorder-destination-street').val(data['google_coordinates']);
            codeDestinationAddress();
        });

        $("#edit-taxiorder-show-route").removeAttr("disabled");
        $("#edit-taxiorder-show-route").removeClass("form-button-disabled");
        $("#edit-taxiorder-show-route").css("display", "block");
      }
    });


    function initialize_map() {
      var mapOptions = {
        zoom: 10,
        scrollwheel: false,
        center: moscow,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

      geocoder = new google.maps.Geocoder();
      directionsService = new google.maps.DirectionsService();
      directionsDisplay = new google.maps.DirectionsRenderer();
    }

    function show_moscow() {
      directionsDisplay.setMap(null);
      markerPickup.setMap(null);
      var mapOptions = {
        zoom: 10,
        center: moscow,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map.setOptions(mapOptions);

      document.getElementById("edit-route-message").innerHTML = '';
      document.getElementsByName("approx_distance")[0].value = '';
      document.getElementsByName("approx_duration")[0].value = '';
      document.getElementsByName("marker_pickup")[0].value = '';
    }

    function mo_bounds() {
      // We prefer addresses within our region.
      var bounds = new google.maps.LatLngBounds(swbound, nebound);
      return bounds;
    }


    function codePickupAddress() {
      directionsDisplay.setMap(null);
      markerPickup.setMap(null);
      $('input[name="approx_distance"]').val('');
      $('input[name="approx_duration"]').val('');
      $('#edit-route-message').html('');

      if ($("#edit-taxiorder-pickup-address-type").val() == 'not_a_poi') {
        var address = $("#edit-taxiorder-pickup-street").val();
        if (address) {
          var bounds = mo_bounds();

          geocoder.geocode({
            'address': address,
            'bounds': bounds
          }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              var position = results[0].geometry.location;
              var mapOptions = {
                zoom: 16,
                center: position
              };
              map.setOptions(mapOptions);

              var markerOptions = {
                map: map,
                position: position
              };
              markerPickup.setOptions(markerOptions);

              position = position.toString();
              // Remove parentheses.
              position = position.substr(1, position.length - 2);

              $('input[name="marker_pickup"]').val(position);

              var formatted_address = results[0].formatted_address;
              $("#edit-taxiorder-pickup-street").val(formatted_address);
            }
            else {
              show_moscow();
            }
          });
        }
        else {
          show_moscow();
        }
      }
      else if ($.isNumeric($("#edit-taxiorder-pickup-address-type").val())) {
        var edit_marker_pickup = $('input[name="marker_pickup"]').val();
        // Separate coordinates.
        arr_marker_pickup = edit_marker_pickup.split(", ");
        var lat = arr_marker_pickup[0];
        var lng = arr_marker_pickup[1];
        var position = new google.maps.LatLng(lat, lng);

        var mapOptions = {
          zoom: 16,
          center: position
        };
        map.setOptions(mapOptions);

        var markerOptions = {
          map: map,
          position: position
        };
        markerPickup.setOptions(markerOptions);
      }
      else {
        show_moscow();
      }
    }

    function codeDestinationAddress() {
      if ($("#edit-taxiorder-destination-street").val()) {
        var edit_marker_pickup = $('input[name="marker_pickup"]').val();
        // Separate coordinates.
        arr_marker_pickup = edit_marker_pickup.split(", ");
        var lat = arr_marker_pickup[0];
        var lng = arr_marker_pickup[1];

        var origin = new google.maps.LatLng(lat, lng);
        var destination;

        var drop_address = $("#edit-taxiorder-destination-street").val();
        if (drop_address) {
          var bounds = mo_bounds();

          geocoder.geocode({
            'address': drop_address,
            'bounds': bounds
          }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              destination = results[0].geometry.location;

              var formatted_address = results[0].formatted_address;
              $("#edit-taxiorder-destination-street").val(formatted_address);

              var request = {
                origin: origin,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
              };

              directionsService.route(request, function(result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                  // Remove pickup address mark.
                  markerPickup.setMap(null);
                  directionsDisplay.setDirections(result);
                  directionsDisplay.setMap(map);

                  var approx_distance = result.routes[0].legs[0].distance.text;
                  var approx_duration = result.routes[0].legs[0].duration.text;
                  $('input[name="approx_distance"]').val(approx_distance);
                  $('input[name="approx_duration"]').val(approx_duration);

                  var route_message = "";
                  if (settings["show_approx"] == 1) {
                    route_message += '<div class="route-message-approx info-message"><p>';
                    route_message += settings["route_message1"] + " " + approx_distance + "<br>";
                    route_message += settings["route_message2"] + " " + approx_duration + "<br>";
                    route_message += '</p><span class="mark"></span></div>';
                  }
                  route_message += '<div class="route-message-desc very-important-message"><p>';
                  route_message += settings["route_message3"];
                  route_message += '</p><span class="mark"></span></div>';
                  $('#edit-route-message').html(route_message);
                }
              });
            }
          });
        }
      }
      else if ($("#edit-taxiorder-pickup-street").val()) {
        codePickupAddress();
      }
      else {
        show_moscow();
      }
    }

    function drawRoute() {
      if ($('input[name="marker_pickup"]').val()) {
        codePickupAddress();
        if ($("#edit-taxiorder-destination-street").val()) {
          $("#edit-taxiorder-show-route").removeAttr("disabled");
          $("#edit-taxiorder-show-route").removeClass("form-button-disabled");
          $("#edit-taxiorder-show-route").css("display", "block");
          codeDestinationAddress();
        }
      }
    }


  });
} (jQuery));
